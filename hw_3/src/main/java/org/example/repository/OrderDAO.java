package org.example.repository;

import org.example.entity.Good;
import org.example.entity.Order;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Repository
public class OrderDAO {
    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    EntityManager entityManager;


    public Optional<Order> getOrderById(Long id) {
        Optional<Order> order = Optional.ofNullable(entityManager.find(Order.class, id));
        return Optional.of(order).orElseThrow(NoSuchElementException::new);
    }

    @Transactional
    public Long save(Order order) {
        entityManager.persist(order);
        return order.getId();
    }

    public void addItems(List<Good> items, Long orderId) {
        Order order = getOrderById(orderId).orElseThrow();
        order.setGoods(items);
    }

    public List<Good> getItemsFromOrder(Long orderId) {
        return getOrderById(orderId).orElseThrow().getGoods();
    }

    public void updateTotalPrice(Long orderId) {
        Order order = getOrderById(orderId).orElseThrow();
        order.setTotalPrice(order.getGoods().stream().mapToDouble(Good::getPrice).sum());
    }

    public List<Order> getAllOrders(Long userId) {
        return entityManager.createQuery("SELECT order FROM Order order WHERE order.userId=:userId", Order.class)
                .setParameter("userId", userId).getResultList();
    }
}
