package org.example.controllers;

import org.example.entity.User;
import org.example.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Controller
@RequestMapping("/")
public class HomePageController {
    private final UserService userService;

    public HomePageController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public String doPost(HttpServletRequest req) {
        boolean agreement = Optional.ofNullable(req.getParameter("agreement")).filter(s -> s.equals("on")).isPresent();
        if (agreement) {
            User user = userService.getUserByName(req.getParameter("username"));
            user.setAgreement(req.getParameter("agreement").equals("on"));
            req.getSession().setAttribute("user", user);
            return "forward:/shop";
        } else return "error";
    }

    @GetMapping
    public String doGet(HttpServletRequest req) {
        if (Optional.ofNullable(req.getParameter("error")).isPresent()) {
            req.setAttribute("error", "Bad credentials");
        }
        return "index";
    }
}
