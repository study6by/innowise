package org.example.utils;

import lombok.SneakyThrows;
import org.h2.jdbcx.JdbcDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class DBUtil {
    private final JdbcDataSource dataSource;

    public DBUtil(JdbcDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @SneakyThrows
    public Connection getConnection() {
        return dataSource.getConnection();
    }

    public ResultSet getStatement(String sql) {
        try {
            Statement statement = getConnection().createStatement();
            return statement.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResultSet getPreparedStatementWithStringParam(String sql, String a) {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement(sql);
            preparedStatement.setString(1, a);
            return preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResultSet getPreparedStatementWithLongParam(String sql, Long param){
        try {
            PreparedStatement statement = getConnection().prepareStatement(sql);
            statement.setLong(1,param);
            return statement.executeQuery();

        }catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setPreparedStatementWithLongParam(String sql, Long param) {
        try (PreparedStatement statement = getConnection().prepareStatement(sql)) {
            statement.setLong(1, param);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public PreparedStatement getPreparedStatement(String sql) {
        try {
            return getConnection().prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
