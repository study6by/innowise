package org.example.services;

import org.example.entity.User;
import org.example.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserByName(String name) {
        Optional<User> user = userRepository.getUserByName(name);
        if (user.isEmpty()) {
            User newUser = new User();
            newUser.setName(name);
            Long generatedId = save(newUser);
            newUser.setId(generatedId);
            return newUser;
        } else {
            return user.get();
        }
    }

    public Long save(User user) {
        return userRepository.save(user);
    }
}
