<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>
    Hello ${user.name}!
</h1>
<h2>
    Make your order
</h2>
<form method=post action=${pageContext.request.contextPath}/order>
    <button type=submit>enter</button>
</form>
<form method=post action=${pageContext.request.contextPath}/shop>
    <c:forEach items="${goods}" var="current">
        <input type=checkbox id=${current.title} name=items value=${current.id}>
        <label for=${current.title}>${current.title} ${current.price}$ </label><br/>
    </c:forEach>
    <button type=submit>add item</button>
</form>
<ul id="food">
    <c:forEach items="${items}" var="current">
        <li><c:out value="${current.title} ${current.price}$"/></li>
    </c:forEach>
</ul>
</body>
</html>
