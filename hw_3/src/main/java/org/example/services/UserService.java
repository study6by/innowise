package org.example.services;

import org.example.entity.User;
import org.example.repository.UserDAO;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserDAO userDAO;

    public UserService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public User getUserByName(String name) {
        return userDAO.getUserByName(name).orElseThrow();
    }

    public Long save(User user) {
        return userDAO.save(user);
    }
}
