<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>
        Online shop
    </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1>
        Number of your order is: ${order.id}<br/>
        Price is: ${order.totalPrice}$<br/>
    </h1>
    <h2>
        List of goods you ordered:<br/>
        <ul class="list-group" id="food">
            <c:forEach items="${items}" var="current">
                <li class="list-group-item"><c:out value="${current.title} ${current.price}$"/></li>
            </c:forEach>
        </ul>
    </h2>
    <form action="${pageContext.request.contextPath}/logout" method="post">
        <button type="submit" class="btn-primary">Logout</button>
        <input type="hidden"
               name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>
</div>
</body>
</html>
