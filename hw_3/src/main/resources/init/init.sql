CREATE TABLE IF NOT EXISTS `User`
(
    `id`        LONG PRIMARY KEY AUTO_INCREMENT,
    `name`      VARCHAR(50) NOT NULL UNIQUE,
    `password`  VARCHAR(100),
    `agreement` BOOLEAN,
    `role`      VARCHAR(20)
);

MERGE INTO `User` (`id`, `name`, `password`, `role`)
    VALUES (1, 'user', '$2y$12$yPPvj0FFe2ZZCNzl7BEAjeEjR73U.auygyMd9A1yFrKKTgEiDcuLq', 'ROLE_USER'),
           (2, 'admin', '$2y$12$mj9vjon9kuMVw00cE6ke3.zm458UVKOdGWkm8TL7RZd1owOrWv.j2', 'ROLE_ADMIN');

CREATE TABLE IF NOT EXISTS `Orders`
(
    `id`          LONG PRIMARY KEY AUTO_INCREMENT,
    `user_id`     VARCHAR(50) NOT NULL references `User` (`id`),
    `total_price` FLOAT(20)
);

CREATE TABLE IF NOT EXISTS `Good`
(
    `id`    LONG PRIMARY KEY NOT NULL,
    `title` VARCHAR(50)      NOT NULL,
    `price` FLOAT(20)        NOT NULL
);

CREATE TABLE IF NOT EXISTS `Order_Good`
(
    `id`       LONG PRIMARY KEY AUTO_INCREMENT,
    `order_id` LONG(20) NOT NULL references `Orders` (`id`),
    `good_id`  LONG(20) NOT NULL references `Good` (`id`)
);

MERGE INTO `Good` (`id`, `title`, `price`)
    VALUES (1, 'Book', 5.5),
           (2, 'Mobile', 100),
           (3, 'Car', 2000),
           (4, 'House', 30000);