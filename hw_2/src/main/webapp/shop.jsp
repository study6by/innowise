<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Online shop</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1>
        Hello ${user.name}!
    </h1>
    <h2>
        Make your order
    </h2>
    <form method=post action=${pageContext.request.contextPath}/shop>
        <c:forEach items="${goods}" var="current">
            <input type=checkbox id=${current.title} name=items value=${current.id}>
            <label for=${current.title}>${current.title} ${current.price}$ </label><br/>
        </c:forEach>
        <button class="btn btn-lg btn-primary btn-block" type=submit>Add item</button>
        <input type="hidden"
               name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>
    <form method=post action=${pageContext.request.contextPath}/order>
        <button class="btn btn-lg btn-dark btn-primary btn-block" type=submit>Watch order</button>
        <input type="hidden"
               name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>
    <ul class="list-group" id="food">
        <c:forEach items="${items}" var="current">
            <li class="list-group-item"><c:out value="${current.title} ${current.price}$"/></li>
        </c:forEach>
    </ul>
</div>
</body>
</html>
