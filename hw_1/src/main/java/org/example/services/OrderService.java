package org.example.services;

import org.example.entity.Order;
import org.example.repository.OrderRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OrderService {
    private final OrderRepository orderRepository;

    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public float updateOrderTotalPrice(Long orderId) {
        return orderRepository.updateOrderTotalPrice(orderId);
    }

    public void insertIntoOrderGood(Long orderId, Long goodId) {
        orderRepository.insertIntoOrderGood(orderId, goodId);
    }

    public Order getOrderById(Long id) {
        Optional<Order> order = orderRepository.getOrderById(id);
        if (order.isEmpty()) {
            Order newOrder = new Order();
            newOrder.setUserId(id);
            Long generatedId = save(newOrder);
            newOrder.setId(generatedId);
            return newOrder;
        } else {
            return order.get();
        }
    }

    public Long save(Order order) {
        return orderRepository.save(order);
    }
}
