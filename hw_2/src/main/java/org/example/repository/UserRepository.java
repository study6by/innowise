package org.example.repository;

import org.example.entity.User;
import org.example.utils.DBUtil;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Repository
public class UserRepository {
    private final DBUtil dbUtil;
    private static final String GET_USER_BY_NAME_SQL = "SELECT * FROM USER WHERE name=(?)";
    private static final String INSERT_USER_SQL = "INSERT INTO USER(name, agreement) VALUES((?), (?))";

    public UserRepository(DBUtil dbUtil) {
        this.dbUtil = dbUtil;
    }

    public Optional<User> getUserByName(String name) {
        User user = null;
        try {
            ResultSet resultSet = dbUtil.getPreparedStatementWithStringParam(GET_USER_BY_NAME_SQL, name);
            while (resultSet.next()) {
                user = new User();
                user.setId(resultSet.getLong("id"));
                user.setName(name);
                user.setAgreement(resultSet.getBoolean("agreement"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(user);
    }

    public Long save(User user) {
        try {
            PreparedStatement preparedStatement = dbUtil.getPreparedStatement(INSERT_USER_SQL);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setBoolean(2, user.isAgreement());
            preparedStatement.executeUpdate();
            ResultSet rs = dbUtil.getPreparedStatementWithStringParam(GET_USER_BY_NAME_SQL, user.getName());
            if (rs.next()) {
                return rs.getLong("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0L;
    }
}
