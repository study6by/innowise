package org.example.repository;

import org.example.entity.Good;
import org.example.utils.DBUtil;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class GoodRepository {
    private final DBUtil dbUtil;
    private static final String SELECT_GOODS_FROM_ORDER_SQL = "SELECT * FROM Good JOIN Order_Good ON Good.id = Order_Good.good_id WHERE order_id=(?)";

    public GoodRepository(DBUtil dbUtil) {
        this.dbUtil = dbUtil;
    }

    public List<Good> getAllGoodsFromOrder(Long orderId) {
        ArrayList<Good> goods = new ArrayList<>();
        try {
            ResultSet resultSet = dbUtil.getPreparedStatementWithLongParam(SELECT_GOODS_FROM_ORDER_SQL, orderId);
            while (resultSet.next()) {
                Good good = new Good();
                good.setId(resultSet.getLong("id"));
                good.setTitle(resultSet.getString("title"));
                good.setPrice(resultSet.getString("price"));
                goods.add(good);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return goods;
    }

    public List<Good> getAllGoods() {
        ArrayList<Good> goods = new ArrayList<>();
        try {
            ResultSet resultSet = dbUtil.getStatement("SELECT * FROM Good");
            while (resultSet.next()) {
                Good good = new Good();
                good.setId(resultSet.getLong("id"));
                good.setTitle(resultSet.getString("title"));
                good.setPrice(resultSet.getString("price"));
                goods.add(good);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return goods;
    }
}
