package org.example.entity;

import lombok.Data;

@Data
public class Good {
    private Long id;
    private String title;
    private String price;
}
