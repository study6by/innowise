package org.example.servlets;

import org.example.AppConfig;
import org.example.entity.Order;
import org.example.entity.User;
import org.example.services.GoodService;
import org.example.services.OrderService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/shop")
public class ShopPageServlet extends HttpServlet {
    private final static ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
    private final OrderService orderService = context.getBean(OrderService.class);
    private final GoodService goodService = context.getBean(GoodService.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        Optional<Order> orderParam = Optional.ofNullable((Order) req.getSession().getAttribute("order"));
        Optional<User> user = Optional.ofNullable((User) req.getSession().getAttribute("user"));
        Order order = null;
        if (user.isPresent()) {
            User newUser = user.get();
            if (orderParam.isEmpty()) {
                order = new Order();
                order.setUserId(newUser.getId());
                order.setId(orderService.save(order));
            } else {
                order = orderService.getOrderById(newUser.getId());
            }
        }
        if (Optional.ofNullable(req.getParameterValues("items")).isPresent()) {
            for (String s : req.getParameterValues("items")) {
                orderService.insertIntoOrderGood(order.getId(), Long.parseLong(s));
            }
            order.setTotalPrice(orderService.updateOrderTotalPrice(order.getId()));
        }
        req.getSession().setAttribute("goods", goodService.getAllGoods());
        req.getSession().setAttribute("order", order);
        req.getSession().setAttribute("items", goodService.getAllGoodsFromOrder(order.getId()));
        req.getRequestDispatcher("/shop.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.getRequestDispatcher("/shop.jsp").forward(req, resp);
    }
}
