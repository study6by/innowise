package org.example.services;

import org.example.entity.Good;
import org.example.repository.GoodDAO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodService {
    private final GoodDAO goodDAO;

    public GoodService(GoodDAO goodDAO)
    {
        this.goodDAO=goodDAO;
    }

    public List<Good> getAllGoods() {
        return goodDAO.getAllGoods();
    }

    public Good getGoodWithId(Long goodId){
        return goodDAO.getGoodWithId(goodId);
    }
}
