package org.example.controllers;

import org.example.entity.User;
import org.example.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Controller
@SessionAttributes("user")
@RequestMapping("/")
public class LoginController {
    private final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ModelAndView forwardLogin(@RequestParam(value = "agreement", required = false) String agr, @RequestParam("username") String username) {
        ModelAndView modelAndView = new ModelAndView();
        boolean agreement = Optional.ofNullable(agr).filter(s -> s.equals("on")).isPresent();
        if (agreement) {
            User user = userService.getUserByName(username);
            user.setAgreement(true);
            modelAndView.addObject("user", user);
            modelAndView.setViewName("forward:/shop");
        } else {
            modelAndView.setViewName("error");
        }
        return modelAndView;
    }

    @GetMapping
    public ModelAndView getLoginPage(@RequestParam(value = "error", required = false) Object error) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        if (Optional.ofNullable(error).isPresent()) {
            modelAndView.addObject("error", "Bad credentials");
        }
        return modelAndView;
    }
}
