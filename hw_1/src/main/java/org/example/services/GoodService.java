package org.example.services;

import org.example.entity.Good;
import org.example.repository.GoodRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodService {
    private final GoodRepository goodRepository;

    public GoodService(GoodRepository goodRepository) {
        this.goodRepository = goodRepository;
    }

    public List<Good> getAllGoodsFromOrder(Long orderId) {
        return goodRepository.getAllGoodsFromOrder(orderId);
    }

    public List<Good> getAllGoods() {
        return goodRepository.getAllGoods();
    }
}
