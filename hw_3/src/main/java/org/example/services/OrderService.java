package org.example.services;

import org.example.entity.Good;
import org.example.entity.Order;
import org.example.repository.OrderDAO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    private final OrderDAO orderDAO;

    public OrderService(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    public Order getOrderById(Long id) {
        return orderDAO.getOrderById(id).orElseThrow();
    }

    public Long save(Order order) {
        return orderDAO.save(order);
    }

    public void addItems(List<Good> items, Long orderId) {
        orderDAO.addItems(items, orderId);
    }

    public List<Good> getItemsFromOrder(Long orderId){
        return orderDAO.getItemsFromOrder(orderId);
    }

    public void updateTotalPrice(Long orderId){
        orderDAO.updateTotalPrice(orderId);
    }

    public List<Order> getAllOrders(Long userId){
        return orderDAO.getAllOrders(userId);
    }
}
