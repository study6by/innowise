package org.example.repository;

import org.example.entity.Order;
import org.example.utils.DBUtil;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Repository
public class OrderRepository {
    private final DBUtil dbUtil;
    private static final String SELECT_ORDER_SQL = "SELECT * FROM orders ORDER BY id DESC LIMIT 1";
    private static final String UPDATE_TOTAL_PRICE_SQL = "UPDATE orders SET total_price=(select SUM(price) FROM Good JOIN Order_Good ON Good.id = Order_Good.good_id WHERE order_id=(?))";
    private static final String SELECT_TOTAL_PRICE_SQL = "SELECT total_price FROM orders WHERE id=(?)";
    private static final String INSERT_USER_ID_INTO_ORDERS_SQL = "INSERT INTO Orders (user_id) VALUES (SELECT id FROM User WHERE id=(?))";
    private static final String ORDER_GOODS_INSERT_SQL = "INSERT INTO order_good (order_id, good_id) VALUES ((?), (?))";

    public OrderRepository(DBUtil dbUtil) {
        this.dbUtil = dbUtil;
    }

    public float updateOrderTotalPrice(Long orderId) {
        try {
            dbUtil.setPreparedStatementWithLongParam(UPDATE_TOTAL_PRICE_SQL, orderId);
            ResultSet resultSet = dbUtil.getPreparedStatementWithLongParam(SELECT_TOTAL_PRICE_SQL, orderId);
            if (resultSet.next()) {
                return resultSet.getFloat("total_price");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0f;
    }

    public void insertIntoOrderGood(Long orderId, Long goodId) {
        try {
            PreparedStatement ps = dbUtil.getPreparedStatement(ORDER_GOODS_INSERT_SQL);
            ps.setLong(1, orderId);
            ps.setLong(2, goodId);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Optional<Order> getOrderById(Long id) {
        Order order = null;
        try {
            ResultSet resultSet = dbUtil.getStatement(SELECT_ORDER_SQL);
            while (resultSet.next()) {
                order = new Order();
                order.setUserId(id);
                order.setId(resultSet.getLong("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(order);
    }

    public Long save(Order order) {
        try {
            dbUtil.setPreparedStatementWithLongParam(INSERT_USER_ID_INTO_ORDERS_SQL, order.getUserId());
            ResultSet rs = dbUtil.getStatement(SELECT_ORDER_SQL);
            if (rs.next()) {
                return rs.getLong("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0L;
    }
}
