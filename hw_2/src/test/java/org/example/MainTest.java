package org.example;

import org.example.utils.DBUtil;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class MainTest {
    private static final ByteArrayOutputStream output = new ByteArrayOutputStream();
    private static final String connectionTrue = "Connection OK";


    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(output));
    }

    @Test
    public void testGetConnection() {
        Assert.assertEquals(output.toString(),connectionTrue+"\r\n");
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }
}
