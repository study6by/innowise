package org.example.repository;

import org.example.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.NoSuchElementException;
import java.util.Optional;

@Repository
public class UserDAO {
    @PersistenceContext
    private EntityManager entityManager;

    public Optional<User> getUserByName(String name) {
        Optional<User> user = Optional.ofNullable(entityManager.createQuery("SELECT user FROM User user WHERE user.name=:name", User.class)
                .setParameter("name", name).getSingleResult());
        return Optional.of(user).orElseThrow(NoSuchElementException::new);
    }

    public Long save(User user) {
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
        return user.getId();
    }
}
