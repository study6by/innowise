<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Online shop</title>
</head>
<body>
<h1>Error! You need to accept user agreement!</h1>
<a href="${pageContext.request.contextPath}/">Return to login page</a>
</body>
</html>