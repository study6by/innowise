<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>
        Online shop
    </title>
</head>
<body>
<h1>
    Number of your order is: ${order.id}<br/>
    Price is: ${order.totalPrice}$<br/>
</h1>
<h2>
    List of goods you ordered:<br/>
    <ul id="food">
        <c:forEach items="${items}" var="current">
            <li><c:out value="${current.title} ${current.price}$"/></li>
        </c:forEach>
    </ul>
</h2>
</body>
</html>
