package org.example.filters;

import org.example.entity.User;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebFilter(urlPatterns = {"/shop", "/order"}, dispatcherTypes = {DispatcherType.REQUEST, DispatcherType.FORWARD})
public class AgreementFilter implements Filter {
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        Optional<User> user = Optional.ofNullable((User) req.getSession().getAttribute("user"));
        if (user.filter(User::isAgreement).isPresent()) {
            chain.doFilter(request, response);
        } else {
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }
}
