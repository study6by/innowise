package org.example.controllers;

import org.example.entity.Good;
import org.example.entity.Order;
import org.example.entity.User;
import org.example.services.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/order")
public class OrdersController {
    private final OrderService orderService;

    public OrdersController(OrderService orderService){
        this.orderService=orderService;
    }

    @PostMapping
    public ModelAndView postOrderPage(HttpSession session) {
        Optional<List<Good>> items = Optional.ofNullable((List<Good>) session.getAttribute("items"));
        Optional<Order> order = Optional.ofNullable((Order) session.getAttribute("order"));
        Optional<User> user = Optional.ofNullable((User) session.getAttribute("user"));
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("order");
        if (order.isPresent()) {
            items.ifPresent(goods -> modelAndView.addObject("items", goods));
        }
        user.ifPresent(value -> modelAndView.addObject("orders", orderService.getAllOrders(value.getId())));
        return modelAndView;
    }
}
