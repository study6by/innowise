package org.example.repository;

import org.example.entity.Good;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class GoodDAO {
    @PersistenceContext
    private EntityManager entityManager;

    public List<Good> getAllGoods() {
        return entityManager.createQuery("SELECT good FROM Good good", Good.class).getResultList();
    }

    public Good getGoodWithId(Long goodId){
        return entityManager.find(Good.class,goodId);
    }
}
