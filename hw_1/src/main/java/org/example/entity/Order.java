package org.example.entity;

import lombok.Data;

@Data
public class Order {
    private Long id;
    private Long userId;
    private float totalPrice;
}
