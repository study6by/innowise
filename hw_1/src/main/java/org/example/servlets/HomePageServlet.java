package org.example.servlets;

import org.apache.commons.lang3.StringUtils;
import org.example.AppConfig;
import org.example.entity.User;
import org.example.services.UserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/")
public class HomePageServlet extends HttpServlet {
    ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
    private final UserService userService = context.getBean(UserService.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String name = req.getParameter("name");
        boolean agreement = Optional.ofNullable(req.getParameter("agreement")).filter(s -> s.equals("on")).isPresent();
        if (StringUtils.isNotBlank(name) && agreement) {
            User user = userService.getUserByName(name);
            user.setAgreement(true);
            req.getSession().setAttribute("user", user);
            req.getRequestDispatcher("/shop").forward(req, resp);
        } else {
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/index.jsp").forward(req, resp);
    }
}
