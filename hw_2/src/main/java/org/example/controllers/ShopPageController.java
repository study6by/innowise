package org.example.controllers;

import org.example.entity.Order;
import org.example.entity.User;
import org.example.services.GoodService;
import org.example.services.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Controller
@RequestMapping("/shop")
public class ShopPageController {
    private final OrderService orderService;
    private final GoodService goodService;

    public ShopPageController(OrderService orderService, GoodService goodService) {
        this.orderService = orderService;
        this.goodService = goodService;
    }

    @PostMapping
    public String doPost(HttpServletRequest req) {
        Optional<Order> orderParam = Optional.ofNullable((Order) req.getSession().getAttribute("order"));
        Optional<User> user = Optional.ofNullable((User) req.getSession().getAttribute("user"));
        Order order = null;
        if (user.isPresent()) {
            User newUser = user.get();
            if (orderParam.isEmpty()) {
                order = new Order();
                order.setUserId(newUser.getId());
                order.setId(orderService.save(order));
            } else {
                order = orderService.getOrderById(newUser.getId());
            }
        }
        if (Optional.ofNullable(req.getParameterValues("items")).isPresent()) {
            for (String s : req.getParameterValues("items")) {
                orderService.insertIntoOrderGood(order.getId(), Long.parseLong(s));
            }
            order.setTotalPrice(orderService.updateOrderTotalPrice(order.getId()));
        }
        req.getSession().setAttribute("goods", goodService.getAllGoods());
        req.getSession().setAttribute("order", order);
        req.getSession().setAttribute("items", goodService.getAllGoodsFromOrder(order.getId()));
        return "shop";
    }

    @GetMapping
    public String doGet() {
        return "shop";
    }
}
