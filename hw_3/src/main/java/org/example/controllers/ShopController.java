package org.example.controllers;

import org.example.entity.Good;
import org.example.entity.Order;
import org.example.entity.User;
import org.example.services.GoodService;
import org.example.services.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@SessionAttributes({"items", "order"})
@RequestMapping("/shop")
public class ShopController {
    private final OrderService orderService;
    private final GoodService goodService;

    public ShopController(OrderService orderService, GoodService goodService) {
        this.orderService = orderService;
        this.goodService = goodService;
    }

    @PostMapping
    public ModelAndView postShopPage(@RequestParam(value = "items", required = false) ArrayList<String> itemsId, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("shop");
        Optional<Order> orderParam = Optional.ofNullable((Order) session.getAttribute("order"));
        Optional<User> user = Optional.ofNullable((User) session.getAttribute("user"));

        Order order = null;
        if (user.isPresent()) {
            if (orderParam.isEmpty()) {
                order = new Order();
                order.setUserId(user.get().getId());
                order.setId(orderService.save(order));
            } else {
                order = orderService.getOrderById(orderParam.get().getId());
            }
        }

        if (Optional.ofNullable(itemsId).isPresent()) {
            List<Good> goods = new ArrayList<>();
            for (String id : itemsId) {
                goods.add(goodService.getGoodWithId(Long.parseLong(id)));
            }
            orderService.addItems(goods, order.getId());
            orderService.updateTotalPrice(order.getId());
            orderService.save(order);
        }

        modelAndView.addObject("goods", goodService.getAllGoods());
        modelAndView.addObject("order", order);
        modelAndView.addObject("items", orderService.getItemsFromOrder(order.getId()));
        return modelAndView;
    }
}
