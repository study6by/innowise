CREATE TABLE IF NOT EXISTS `User` (
     `id` INTEGER  PRIMARY KEY AUTO_INCREMENT,
     `login` VARCHAR(50),
     `password` VARCHAR (50),
    `name` VARCHAR(50) NOT NULL UNIQUE,
    `agreement` BOOLEAN NOT NULL
    );
CREATE TABLE IF NOT EXISTS `Orders` (
     `id` INTEGER  PRIMARY KEY AUTO_INCREMENT,
     `user_id` VARCHAR(50) NOT NULL references `User`(`id`),
     `total_price` FLOAT (20)
);
CREATE TABLE IF NOT EXISTS `Good` (
     `id` INTEGER  PRIMARY KEY NOT NULL ,
     `title` VARCHAR(50) NOT NULL,
     `price` FLOAT(20) NOT NULL
);
MERGE INTO `Good` (`id`,`title`,`price`)
VALUES (1,'Book',5.5),
        (2,'Mobile',100),
        (3,'Car',2000),
        (4,'House',30000);
CREATE TABLE IF NOT EXISTS `Order_Good` (
     `id` INTEGER  PRIMARY KEY AUTO_INCREMENT,
     `order_id` VARCHAR(50) NOT NULL references `Orders`(`id`),
     `good_id` VARCHAR(50) NOT NULL references `Good`(`id`)
);